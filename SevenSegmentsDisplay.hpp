#ifndef SEVEN_SEGMENTS_CONTROLLER_H
#define SEVEN_SEGMENTS_CONTROLLER_H

#include <mbed.h>

namespace ufal {

class SevenSegmentsDisplay {
public:
  SevenSegmentsDisplay(PinName p7, PinName p6, PinName p5, PinName p4,
                       PinName p3, PinName p2, PinName p1, PinName p0);
  SevenSegmentsDisplay(BusOut *display);
  ~SevenSegmentsDisplay();
  void calibration();
  void turnOnDot();
  void turnOffDot();
  void write(unsigned char value);
  unsigned char read();
  void operator=(unsigned char value);
  unsigned char operator()(unsigned char value);

private:
  BusOut *m_display;
};
};
#endif // SEVEN_SEGMENTS_CONTROLLER_H
