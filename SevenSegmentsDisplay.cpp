#include "SevenSegmentsDisplay.hpp"

namespace ufal {

unsigned char ssCode[] = {
    /*case 0:*/ 0b01110111,
    /*case 1:*/ 0b00010100,
    /*case 2: */ 0b10110011,
    /*case 3: */ 0b10110110,
    /*case 4: */ 0b11010100,
    /*case 5: */ 0b11100110,
    /*case 6: */ 0b11100111,
    /*case 7: */ 0b00110100,
    /*case 8: */ 0b11110111,
    /*case 9: */ 0b11110110,
    /*case 0xa: */ 0b11110101,
    /*case 0xb: */ 0b11000111,
    /*case 0xc: */ 0b01100011,
    /*case 0xd: */ 0b10010111,
    /*case 0xe: */ 0b11100011,
    /*case 0xf: */ 0b11100001};

SevenSegmentsDisplay::SevenSegmentsDisplay(PinName p7, PinName p6, PinName p5,
                                           PinName p4, PinName p3, PinName p2,
                                           PinName p1, PinName p0) {
  m_display = new BusOut(p7, p6, p5, p4, p3, p2, p1, p0);
}

SevenSegmentsDisplay::SevenSegmentsDisplay(BusOut *display) {
  m_display = display;
}

void SevenSegmentsDisplay::calibration() {
  for (int count = 0; count < 8; count++, wait(1)) {
    m_display->write(1 << count);
  }
}

SevenSegmentsDisplay::~SevenSegmentsDisplay() { delete m_display; }

void SevenSegmentsDisplay::turnOnDot() { (*m_display)[3] = 1; }
void SevenSegmentsDisplay::turnOffDot() { (*m_display)[3] = 0; }
void SevenSegmentsDisplay::write(unsigned char value) {
  printf("ssCode %x\n", ssCode[value]);
  if (value <= 15) {
    m_display->write(ssCode[value]);
  } else {
    m_display->write(0b11111111);
  }
}

unsigned char SevenSegmentsDisplay::read() { return m_display->read(); }

void SevenSegmentsDisplay::operator=(unsigned char value) {
  this->write(value);
}

unsigned char SevenSegmentsDisplay::operator()(unsigned char value) {
  return m_display->read();
}
};
